# stockpile

Stockpile is a program to manage [Dwarf Fortress](http://www.bay12games.com/dwarves/) installations
using version control. The goal is to allow a user to install DF once, create a git repository in that
installation, and then install mods and tilesets on various branches of the repository. When a user wants
to switch to a different mod, they simply switch the branch, and then any launcher or script that they
have in place to launch their copy of DF will now run on top of the other branch.

I wrote stockpile because I wanted a solution for managing Dwarf Fortress mods that would *not*
require me to maintain multiple installations of DF on my system. It was designed to integrate well
with the Fedora packages of Dwarf Fortress that I maintain-- packages where a single copy of all
the data and raw files are installed on the root filesystem and symbolically linked into each user's home
directory. This means that updates to the package automatically update each user's DF installation,
which is very convenient if you only want to play vanilla but less convenient if you want to deal
with mods and tilesets.

There are three obvious problems that needed to be resolved:

* Installing mods would require modification of files owned by root (in e.g. /usr/share/dwarffortress).
* Installed mods would be permanent, i.e. there'd be no way to switch back to vanilla, at least until...
* Installed mods would get overwritten by package updates the next time DF updated, and your modded
copy of the game would no longer work.

Stockpile solves these problems as follows:

* Now, installing mods can be done by creating a new branch in your local DF installation in your home
directory.
* Switching back to vanilla is as easy as switching to the master branch.
* All non-master branches will be un-symlinked; that is to say, instead of having symbolic links to /usr/share/dwarffortress/raw,
when you run "stockpile branch" to create a new branch, the symbolic link is removed and the raw files copied.
Thus DF updates will not update your modded copies of DF but continue to update the vanilla copy.

While primarily useful for system-packaged Dwarf Fortress (as available on Fedora, Arch, and possibly
other distributions too), managing modded installations via version control might potentially
be of interest to other users too.
Note that stockpile is a command line program that's intended to be a lightweight solution to the
problem of installation management.

## Installation

Stockpile is available, on Fedora (and select other RPM distributions, like CentOS), from my
[repository here](https://www.acm.jhu.edu/~bjr/pages/dwarf-fortress-for-fedora.html). It can
be installed by:

```
wget -P /etc/yum.repos.d/ https://www.acm.jhu.edu/~bjr/fedora/dwarffortress/dwarffortress.repo
dnf install stockpile
```

The Dwarf Fortress packages from that repository have been modified slightly to ensure compatibility
with Stockpile.

(In the future, this package will hopefully be available from [RPM Fusion](https://rpmfusion.org/) as well).

Users of other Linux distributions can install this like any other Python module. As the name 'stockpile'
is sufficiently generic as to have already been claimed on PyPI, I've not registered this module
there, however.

Note that the ```git``` package (e.g. ```/usr/bin/git```) must be installed on your system too,
in addition to the dependency on pygit2. This is because, when implementing the ```stockpile list-branches```
command, I simply shelled out to run ```git branch``` in the Dwarf Fortress install directory rather
than rewriting my own. In the future this will hopefully be fixed.

This is, more or less, Linux only. I have not attempted to test it on Windows and have little interest
in the prospect. My guess is that it will work provided that the git binary is located on your PATH,
however I am not sure. As this is an open source project, should anyone be interested in Windows support
patches are welcome. :)

## Usage

Stockpile assumes that you have installed Dwarf Fortress somewhere. By default it's located in
```~/.dwarffortress/```, though this can be overridden by passing a command-line flag (and,
later, by setting a configuration option). Once you have a DF installation, you can run the
following command to set up version control:

```
stockpile [-d /path/to/dwarffortress] setup
```

This will turn your DF installation into a git repository. If you then wish to create a moddable
branch, you can run the following command:

```
stockpile [-d /path/to/dwarffortress] branch [branchname]
```

This will commit any loose files (e.g. saves, modified configuration, etc.) and then create a new
branch called "branchname". Any symbolic links out of the git repository will be undone and their
contents copied in their place. You can then install mods, enable a tileset, delete saves, edit
config, etc. in the branch without messing with the master branch.

New branches will be created with a .stockpile file in them. This is to enable any utilities
or launcher scripts to distinguish between modded Dwarf Fortress and vanilla Dwarf Fortress
(specifically, the launcher scripts in the Fedora packages).

To switch back and forth between branches, use the following command:

```
stockpile [-d /path/to/dwarffortress] switch [branchname]
```

Switching to "master" will switch back to the master / vanila branch. Note that whenever you
run "switch" *or* "branch", the current state of the repository will be committed. You should
never have to manually commit files.

To see the list of branches (and what branch you're currently on), you can run the following:

```
stockpile [-d /path/to/dwarffortress] list-branches
```

To delete a branch, you can use the "delete" command as follows. Unless the -y flag is
passed, delete will first prompt for verification. Note that delete will switch back to the
master branch if you were on the branch that you wanted deleted.

```
stockpile [-d /path/to/dwarffortress] delete [-y] [branchname]
```

The git repository in your DF installation is, for all other intents and purposes, just a normal
git repository and can be interacted with git on the command line. This means that should you
wish to back up your DF installations you could configure a remote and push your install and all
its branches and commits to that remote. Stockpile will not currently support this by default;
this is something an advanced user could make work, however.

## Credits

This program was developed by me, Ben Rosser <rosser.bjr@gmail.com>, under the MIT license.
See LICENSE for the full text.

Dwarf Fortress is a proprietary, free to redistribute game developed by Tarn "Toady One" Adams.
If you like Dwarf Fortress, you should make a donation to [Bay 12 Games](http://www.bay12games.com/champions.html)!

