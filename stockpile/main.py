#!/usr/bin/env python3

import argparse
import os
import sys

from stockpile import commands

def main():
	# Load all commands (i.e. Python modules in boiler.commands)
	# Later we can have a plugin interface or something.
	plugins = commands.loadCommands()

	# Create argument parser.
	parser = argparse.ArgumentParser(prog='stockpile', description='A tool for managing Dwarf Fortress installations.')

	default_dfdir = os.path.join(os.getenv('HOME'), ".dwarffortress")
	parser.add_argument("-d", "--df-dir", dest='dfdir', default=default_dfdir, help="The location of your managed DF installation.")

	# Create subparsers.
	subparsers = parser.add_subparsers(title="Subcommand", description="Which stockpile subcommand to run.")
	for command in plugins.values():
		command.createSubparser(subparsers)

	# Now, execute the default function for whichever path was chosen.
	args = parser.parse_args()
	try:
		args.func(args)
	except AttributeError:
		print("Please select a command. Run stockpile -h to see a list of all commands.")

if __name__ == '__main__':
	main()
