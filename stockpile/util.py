# Utility routines for stockpile package.

import os

def detectDF(dfdir):
	"""	Returns true if Dwarf Fortress installation is detected, false otherwise."""
	# Current detectdf criteria = existence of ./raw/, ./data/, ./data/init/
	detected = True

	if not os.path.exists(os.path.join(dfdir, "data")):
		detected = False

	if not os.path.exists(os.path.join(dfdir, "data", "init")):
		detected = False

	if not os.path.exists(os.path.join(dfdir, "raw")):
		detected = False

	return detected
