# stockpile git interface.

import pygit2

def getAuthorInfo():
	"""	Returns the default git author information used by this package."""
	author = pygit2.Signature("Dwarf Fortress", "df@localhost")
	return author

def createRepo(dfdir):
	"""	 Initializes a repository in dfdir if it does not already exist."""
	repo = pygit2.init_repository(dfdir)

	# Add everything to index.
	repo.index.add_all()
	repo.index.write()
	tree = repo.index.write_tree()

	# Create a commit. Maybe a better default author would be good?
	author = getAuthorInfo()
	repo.create_commit("refs/heads/master", author, author, "Initial commit of Dwarf Fortress directory.", tree, [])

def checkForRepo(dfdir):
	"""	Checks if a git repository exists at dfdir, return true if it does, false otherwise."""
	try:
		pygit2.discover_repository(dfdir)
	except:
		return False
	return True

def createCommit(dfdir):
	"""	Creates a commit on the current branch."""
	repo = pygit2.repository.Repository(dfdir)
	repo.index.add_all()
	repo.index.write()
	tree = repo.index.write_tree()

	author = getAuthorInfo()
	repo.create_commit(repo.head.name, author, author, "Update repository.", tree, [repo.head.target])

def createBranch(dfdir, branch_name):
	"""	Creates a new branch."""
	repo = pygit2.repository.Repository(dfdir)
	repo.create_branch(branch_name, repo.head.get_object())

def deleteBranch(dfdir, branch_name):
	"""	Deletes a branch. Returns true if successful, false otherwise."""
	repo = pygit2.repository.Repository(dfdir)

	try:
		reference = repo.lookup_branch(branch_name)
	except pygit2.GitError:
		return False

	reference.delete()
	return True


def switchToBranch(dfdir, branch_name):
	"""	Switches to branch branch_name if it exists. Returns true if successful, false otherwise."""
	repo = pygit2.repository.Repository(dfdir)

	try:
		reference = repo.lookup_branch(branch_name)
	except pygit2.GitError:
		return False

	# The reference pointing to the branch_name.
	repo.checkout(reference)
	return True

def checkIfClean(dfdir):
	"""	Check if the current tree is clean using pygit2."""
	repo = pygit2.repository.Repository(dfdir)
	for filepath, flags in repo.status().items():
		if flags != pygit2.GIT_STATUS_CURRENT and flags != 16384:
			return False
	return True

def checkIfBranchExists(dfdir, branch_name):
	"""	Check if branch_name exists in the dfdir git repository, returns True if it does, False if it does not."""
	repo = pygit2.repository.Repository(dfdir)
	try:
		reference = repo.lookup_branch(branch_name)
	except pygit2.GitError:
		return False
	if reference is None:
		return False
	return True

def getCurrentBranchName(dfdir):
	"""	Returns the (shorthand) name of the current branch."""
	repo = pygit2.repository.Repository(dfdir)
	return repo.head.shorthand
