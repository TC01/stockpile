# The wheelbarrrow list-branches command. Lists the branches in the repository.
# Currently, this shells out to git and runs 'git branch' in args.dfdir. This is not ideal.
# As a result /usr/bin/git (or equivalent) is assumed to be installed.

# In the future, reimplement using pygit2 commands.

import os
import sys

from stockpile import git

def getName():
	return "list-branches"

def main(args):
	"""	Entrypoint function that gets executed by stockpile list-branches."""
	if not git.checkForRepo(args.dfdir):
		print("Error: you must run 'stockpile setup' first.")
		sys.exit(1)

	# Change to args.dfdir, print output of git branch, return to current dir.
	curdir = os.getcwd()
	os.chdir(args.dfdir)
	os.system("git branch")
	os.chdir(curdir)

def createSubparser(subparsers):
	parser = subparsers.add_parser("list-branches")
	parser.set_defaults(func=main)
