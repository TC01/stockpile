# The "stockpile switch" command.
# Commits the current state of ~/.dwarffortress/ and then switches to requested branch.

import sys

from stockpile import git

def getName():
	return "switch"

def main(args):
	"""	Entrypoint function that gets executed by stockpile branch."""
	if args.branch_name == "":
		print("Error: you must specify the branch name to switch to.")
		sys.exit(1)

	if not git.checkForRepo(args.dfdir):
		print("Error: you must run 'stockpile setup' first.")
		sys.exit(1)

	# Make sure branch name exists.
	if not git.checkIfBranchExists(args.dfdir, args.branch_name):
		print("Error: branch %s does not exist! First create it using 'stockpile branch'." % args.branch_name)

	# Commit current state to current branch, then switch to the requested branch.
	if not git.checkIfClean(args.dfdir):
		git.createCommit(args.dfdir)

	git.switchToBranch(args.dfdir, args.branch_name)

	if args.branch_name == "master":
		print("Switched to vanilla Dwarf Fortress (master).")
	else:
		print("Switched to moddable Dwarf Fortress branch (%s)." % args.branch_name)

def createSubparser(subparsers):
	parser = subparsers.add_parser("switch")
	parser.set_defaults(func=main)

	parser.add_argument('branch_name', type=str, help="Name of branch to switch to.", default="")

