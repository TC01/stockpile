# Plugin/command loading interface for stockpile.

import copy
import imp
import os
import sys

def loadCommands():
	commands = {}
	locations = []
	locations.insert(0, os.path.join(getScriptLocation()))

	# Get a list of all possible plugin names
	names = []
	for location in locations:
		for path, dirs, files in os.walk(location):
			if path == location:
				for filename in files:
					if not (".pyc" in filename or "__init__" in filename):
						filename = filename[:-len(".py")]
						names.append(filename)

	# Now, use imp to load all the plugins we specified
	loadPath = copy.copy(locations)
	loadPath.extend(__path__)
	for name in names:
		try:
			test = sys.modules[name]
		except KeyError:
			fp, pathname, description = imp.find_module(name, loadPath)
			try:
				plugin = imp.load_module(name, fp, pathname, description)
				pluginName = plugin.getName()
				commands[pluginName] = plugin
			finally:
				if not fp is None:
					fp.close()

	return commands

# Helper functions

def getScriptLocation():
	"""Helper function to get the location of a Python file."""
	location = os.path.abspath("./")
	if __file__.rfind("/") != -1:
		location = __file__[:__file__.rfind("/")]
	return location
