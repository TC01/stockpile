# The "stockpile branch" command.
# Commits the current state of ~/.dwarffortress/ and then creates a new branch.
# Then, in that new branch, unlinks the symbolic links to create a local, not system, installation.

# Then commits the changes.

import os
import sys

from stockpile import cplink
from stockpile import git

def getName():
	return "branch"

def main(args):
	"""	Entrypoint function that gets executed by stockpile setup."""
	if args.branch_name == "":
		print("Error: you must specify a name for your new branch.")
		sys.exit(1)

	if not git.checkForRepo(args.dfdir):
		print("Error: you must run 'stockpile setup' first.")
		sys.exit(1)

	# Check that the branch name isn't already in use.
	if git.checkIfBranchExists(args.dfdir, args.branch_name):
		print("Error: a branch already exists with this name! Use 'stockpile switch %s' to switch to this branch." % args.branch_name)
		sys.exit(1)

	# Commit current state to current branch, switch to master, create a new branch, and switch to that branch.
	if not git.checkIfClean(args.dfdir):
		git.createCommit(args.dfdir)
	git.switchToBranch(args.dfdir, "master")
	git.createBranch(args.dfdir, args.branch_name)
	git.switchToBranch(args.dfdir, args.branch_name)

	# Now use cplink to unlink the branch and create a file to note this *isn't* a system install.
	cplink.cplink_recursive(args.dfdir)
	with open(os.path.join(args.dfdir, '.stockpile'), 'w') as dwarffile:
		dwarffile.write("This file exists to note that this DF install/branch was created by Stockpile.\n")
		dwarffile.write("It will not be automatically updated by updates to the DF package, and the DF launcher script should not attempt to do so.\n")

	# Create a new commit.
	git.createCommit(args.dfdir)

	print("Created new branch (%s) and collapsed all symbolic links." % args.branch_name)
	print("On this branch, you can now modify files, install mods and tilesets, etc. in %s." % args.dfdir)
	print("Use 'stockpile switch master' to switch back to vanilla.")

def createSubparser(subparsers):
	parser = subparsers.add_parser("branch")
	parser.set_defaults(func=main)

	parser.add_argument('branch_name', type=str, help="Name of branch to create.", default="")

