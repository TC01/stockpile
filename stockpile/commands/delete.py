# The "stockpile delete" command.
# Deletes the current branch. By default, asks the user for verification first.

import os
import sys

from stockpile import cplink
from stockpile import git

def getName():
	return "delete"

def main(args):
	"""	Entrypoint function that gets executed by stockpile setup."""
	if args.branch_name == "":
		print("Error: you must specify the name of the branch to delete.")
		sys.exit(1)
	elif args.branch_name == "master":
		print("Error: you cannot delete the master branch.")
		sys.exit(1)

	if not args.assume_yes:
		# There's got to be a library for this.
		confirm = None
		while confirm != 'y' and confirm != 'n':
			sys.stdout.write("Are you sure you want to delete branch '%s' [y/n]? " % args.branch_name)
			confirm = input().lower()
			if confirm != 'y' and confirm != 'n':
				print("Please enter 'y' or 'n'")
		if confirm == 'n':
			sys.exit(1)

	if not git.checkForRepo(args.dfdir):
		print("Error: you must run 'stockpile setup' first.")
		sys.exit(1)

	# Check that the branch name _is_ in use, if so, delete it.
	if not git.checkIfBranchExists(args.dfdir, args.branch_name):
		print("Error: a branch does not exist with this name.")
		sys.exit(1)

	if git.getCurrentBranchName(args.dfdir) == args.branch_name:
		git.switchToBranch(args.dfdir, "master")

	git.deleteBranch(args.dfdir, args.branch_name)

	print("Deleted branch %s. If you were on this branch, you are now on master instead." % args.branch_name)

def createSubparser(subparsers):
	parser = subparsers.add_parser("delete")
	parser.set_defaults(func=main)

	parser.add_argument('branch_name', type=str, help="Name of branch to delete.", default="")
	parser.add_argument('-y', '--assume-yes', action='store_true', dest="assume_yes", help="Do not prompt for verification.")

