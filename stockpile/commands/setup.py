# The "stockpile setup" command. Does basic setup, i.e. turns ~/.dwarffortress/
# into a git repository.

import subprocess

import sys

from stockpile import git
from stockpile import util

def getName():
	return "setup"

def main(args):
	"""	Entrypoint function that gets executed by stockpile setup."""

	# Try to detect DF install first
	if not util.detectDF(args.dfdir):
		print("Error: we did not detect a Dwarf Fortress installation at '%s'." % args.dfdir)
		sys.exit(1)

	if not git.checkForRepo(args.dfdir):
		git.createRepo(args.dfdir)

	print("Created git repository in %s tracking current vanilla installation on branch 'master'." % args.dfdir)
	print("Now, run 'stockpile branch [name]' to create a moddable branch.")
	print("Use 'stockpile switch [name]' to switch from branch to branch and 'stockpile list-branches' to look at your branches.")

def createSubparser(subparsers):
	parser = subparsers.add_parser("setup")
	parser.set_defaults(func=main)

	# init-df takes no special arguments.
