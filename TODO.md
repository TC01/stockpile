# Future Development

This page lists things that I'd like to work on but haven't gotten around to implementing yet.

### A configuration file parser

Ideally, if you want to change the location of your DF directory, you shouldn't need to keep
passing it on the command line. This should be really simple and is likely a feature for 0.2.
A config file would probably be looked for in /etc/stockpile.conf and overriddable in
~/.config/stockpile.conf. This would be another area where non-Linux support may run into
issues.

### Unit tests

There may be corner cases not handled by the code that I haven't ran into yet. Unit tests
would be really nice for the core modules (git.py, util.py, and cplink.py). This is also
a 0.2 feature.

### Automatic installation of DF

If specified via command line option, downloading a DF version from the Bay 12 site and
unpacking it would be a useful feature. I already have code to do this elsewhere but it
needs to be updated now that the format of the downloads page has changed with 64-bit
releases.

This is not really a priority for me, given the existence of Fedora packages, but would
be nice to have for a future release.

### Support for remotes

It'd be nice to be able to use stockpile to synchronize DF installations across systems.
I'm not sure how this could be implemented reasonably though. This is a feature that may
never get worked on.
